# My Career Radar

I've always believed that everyone should have a career *radar* in their heads when thinking about their own career development.

So I decided to create my own with the top 100 things I know that came to my mind and see where they land on my career radar.